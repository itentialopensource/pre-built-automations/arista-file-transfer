<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [Arista - EOS - IAG](https://gitlab.com/itentialopensource/pre-built-automations/arista-eos-iag)

# Arista File Transfer

## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

This Pre-Built Automation contains a workflow designed to perform file transfer for Arista EOS devices. The workflow utilizes FTP on the Arista device.

_Estimated Run Time_: 10 minutes (depending on file size and network speed)

## Installation Prerequisites

Users must satisfy the following pre-requisites:

* Itential Automation Platform
  * `^2022.1`

## Requirements

This Pre-Built requires the following:

<!-- Unordered list highlighting the requirements of the Pre-Built -->
<!-- EXAMPLE -->
* Arista device onboarded to either NSO or IAG
* NSO or IAG adapter

## Features

The main benefits and features of the Pre-Built are outlined below.

* Validates file intergrity (md5) after download (optional)
* Allow zero-touch mode of operation


## Future Enhancements

Add support for other protocols such as scp

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built: Navigate to the Operations Manager app in IAP, then select to run the Arista File Transfer.
Fill up the form with the following details:

* Perform MD5 check (checkbox) - compare to entered MD5
* Zero Touch (checkbox) - eliminate user interactions
* Device name
* FTP remote server information
  * Username - remote file server username used with FTP (Ex. ftpuser)
  * Password - remote file server password used with FTP
  * IP Address - remote file server IP address (Ex. 192.168.1.25)
  * Path - remote file server path to file (Ex. /home/)
  * File Name - remote server file name (Ex. foo.bar)
<!-- Explain the main entrypoint(s) for this Pre-Built: Automation Catalog item, Workflow, Postman, etc. -->

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
