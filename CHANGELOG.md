
## 0.0.14 [02-05-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/arista-file-transfer!10

---

## 0.0.13 [02-04-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/arista-file-transfer!6

---

## 0.0.12 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/arista-file-transfer!5

---

## 0.0.11 [06-20-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/arista-file-transfer!4

---

## 0.0.10 [05-10-2022]

* Update manifest.json

See merge request itentialopensource/pre-built-automations/staging/arista-file-transfer!3

---

## 0.0.9 [05-10-2022]

* Update manifest.json

See merge request itentialopensource/pre-built-automations/staging/arista-file-transfer!3

---

## 0.0.8 [05-09-2022]

* Update manifest.json

See merge request itentialopensource/pre-built-automations/staging/arista-file-transfer!3

---

## 0.0.7 [05-02-2022]

* Update manifest.json

See merge request itentialopensource/pre-built-automations/staging/arista-file-transfer!3

---

## 0.0.6 [04-29-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/arista-file-transfer!2

---

## 0.0.5 [04-29-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/arista-file-transfer!1

---

## 0.0.4 [04-29-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/arista-file-transfer!1

---

## 0.0.3 [04-29-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/arista-file-transfer!1

---

## 0.0.2 [04-28-2022]

* Bug fixes and performance improvements

See commit f4c867a

---

## 0.0.7 [04-21-2022]

* Bug fixes and performance improvements

See commit 3983a14

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n
